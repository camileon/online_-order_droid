import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:onlineorder/util/ui_data.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {


  bool isLoading = false;
  bool isAutoValidate = false;
  String status = "";

  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();



  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
//            Positioned(top: 0, left: 0, child: CurvedLeftShadow()),
//            Positioned(top: 0, left: 0, child: CurvedLeft()),
//            Positioned(bottom: 0, left: 0, child: CurvedRightShadow()),
            Container(
              height: size.height,
              width: size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  size.height > 680 ? SizedBox(height: 10.0,) : SizedBox(),
                  Image(
                    width: Get.width * 0.5,
                    image: Image.asset("assets/images/px_logo_transparent.png").image,
                  ),
                  SizedBox(height: Get.height < 680 ? 15.0 : 20.0,),
                  Container(
                    padding: EdgeInsets.only(bottom: 30.0),
                    child: Text(
                      "login".trArgs(),
                      style: GoogleFonts.varelaRound(
                          textStyle: TextStyle(
                              color: Colors.amber[600],
                              fontSize: Get.height < 680 ? 18.0 : 22.0,
                              fontWeight: FontWeight.w800
                          )
                      ),
                    ),
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        height: Get.height < 680 ? 105.0 : 120.0,
                        padding: EdgeInsets.only(left: 15.0),
                        margin: EdgeInsets.only(right: 40.0),
                        decoration: BoxDecoration(
                          color: Colors.grey[800],
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 20.0,
                            )
                          ],
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(50.0),
                            bottomRight: Radius.circular(50.0),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            TextFormField(
                              inputFormatters: [WhitelistingTextInputFormatter(RegExp(r'[^ ]'))],
                              controller: emailController,
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      color: Colors.grey[400],
                                      fontSize: Get.height < 680 ? 12.0 : 15.0,
                                      fontWeight: FontWeight.w500)),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(
                                    0.0,
                                    15.0,
                                    60.0,
                                    15.0
                                ),
                                icon: Icon(
                                  Icons.person,
                                  size: Get.height < 680 ? 18.0 : 22.0,
                                ),
                                hintText: "email".trArgs(),
                                border: InputBorder.none,
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.black12,
                                ),
                              ),
                            ),
                            TextFormField(
                              controller: passwordController,
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      color: Colors.grey[400],
                                      fontSize: Get.height < 680 ? 12.0 : 15.0,
                                      fontWeight: FontWeight.w500)),
                              obscureText: true,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(
                                    0.0,
                                    15.0,
                                    60.0,
                                    15.0
                                ),
                                icon: Icon(
                                  Icons.lock,
                                  size: Get.height < 680 ? 18.0 : 22.0,
                                ),
                                hintText: "password".trArgs(),
                                border: InputBorder.none,
                              ),
//                              enableSuggestions: false,
                              onTap: (){
                                print("Tap");
                              },
                              onChanged: (val) {
                                final trimVal = val.trim();
                                if (val != trimVal)
                                  setState(() {
                                    emailController.text = trimVal;
                                    emailController.selection = TextSelection.fromPosition(TextPosition(offset: trimVal.length));
                                  });
                              },
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        top: 25,
                        right: 15,
                        child: RaisedButton(
                          shape: CircleBorder(
                              side: BorderSide(
                                  color: Colors.transparent
                              )
                          ),
                          child: Container(
                            padding: EdgeInsets.all(15.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Colors.amber[800],
                                  Colors.amber
                                ],
                              ),
                            ),
                            child: Icon(
                              Icons.arrow_forward,
                              size: Get.height < 680 ? 30.0 : 40.0,
                              color: Colors.black54,
                            ),
                          ),
                          elevation: 20.0,
                          splashColor: Colors.amber[900],
                          onPressed: (){
                            if(!EmailValidator.validate(emailController.text)) {
                              Get.snackbar(
                                '', '',
                                titleText: Text(
                                  "Error !",
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w700)),
                                ),
                                messageText: Text(
                                  UiData.pleaseEnterValidEmail,
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          color: Colors.black45,
                                          fontWeight: FontWeight.w600)),
                                ),
                                backgroundColor: Colors.redAccent,
                                isDismissible: true,
                                duration: Duration(seconds: 5),
                                snackPosition: SnackPosition.BOTTOM,
                                margin: EdgeInsets.only(
                                    left: 15,
                                    right: 15,
                                    bottom: 15.0
                                ),
                              );
                            } else {
//                              setState(() {
//                                  isLoading = true;
//                                });
//                                login();
                              if(passwordController.text.length > 6 ) {
                                setState(() {
                                  isLoading = true;
                                });
                                login();
                              } else {
                                Get.snackbar(
                                  '', '',
                                  titleText: Text(
                                    "Error !",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            color: Colors.black54,
                                            fontWeight: FontWeight.w700)),
                                  ),
                                  messageText: Text(
                                    "Password  should be more then 6 characters",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            color: Colors.black45,
                                            fontWeight: FontWeight.w600)),
                                  ),
                                  backgroundColor: Colors.redAccent,
                                  isDismissible: true,
                                  duration: Duration(seconds: 5),
                                  snackPosition: SnackPosition.BOTTOM,
                                  margin: EdgeInsets.only(
                                      left: 15,
                                      right: 15,
                                      bottom: 15.0
                                  ),
                                );
                              }
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.symmetric(
                      vertical: 25.0,
                      horizontal: 30.0,
                    ),
                    child: InkWell(
                      child: Text(
                        "label.forgot.password".trArgs(),
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                color: Colors.amber[600],
                                fontSize: Get.height < 680 ? 12.0 : 18.0,
                                fontWeight: FontWeight.w700)),
                      ),
                      onTap: (){
//                        forgetPassword();
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
//                      vertical: 25.0,
                      horizontal: 30.0,
                    ),
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            "Don't have an account ? ",
                            style: TextStyle(color: Colors.amber[600]),
                          ),
                          InkWell(
                            onTap: () {
                              Get.toNamed('/signup_personal_information');
                            },
                            child: Text(
                              "Sign Up",
                              style: TextStyle(
                                fontSize: Get.height < 680 ? 12.0 : 18.0,
                                color: Colors.amber[600],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                height: Get.height > 680 ? 150 : 90,
                child: WaveWidget(
                  config: CustomConfig(
                    gradients: [
                      [Colors.red, Color(0xEEF44336)],
                      [Colors.amber[800], Color(0x77E57373)],
                      [Colors.amber[600], Color(0x66FF9800)],
                      [Colors.amber, Color(0x55FFEB3B)]
                    ],
                    durations: [35000, 19440, 10800, 6000],
                    heightPercentages: [0.20, 0.23, 0.25, 0.30],
                    blur: MaskFilter.blur(BlurStyle.solid, 10),
                    gradientBegin: Alignment.bottomLeft,
                    gradientEnd: Alignment.topRight,
                  ),
                  waveAmplitude: 0,
//                    backgroundColor: Colors.blue,
                  size: Size(
                    Get.width,
                    Get.height > 680 ? 150 : 100,
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                width: Get.width,
                height: 30,
                child: Center(
                  child: Text(
                    "Powered By Cdev",
                    style: TextStyle(
                      color: Colors.grey[900],
                      fontSize: Get.height < 680 ? 8.0 : 12.0,
//                          fontWeight: FontWeight.w800
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }



  login() async {
  }


}
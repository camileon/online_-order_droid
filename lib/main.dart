import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:onlineorder/pages/login.dart';
import 'package:onlineorder/util/theme_data_util.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runZonedGuarded(() {
      runApp(GetMaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute:"/login",
        theme: new ThemeDataUtil().getInitialTheme(),
        locale: Locale('en'),
        getPages: [
          GetPage(
            name: '/login',
            page: () => Login(),
            transition: Transition.native,
            transitionDuration: Duration(milliseconds: 1000),
          ),
        ],
      ));
    }, (error, stackTrace) {
    });
  });
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UiData {
  static String baseEnvironment="qa";
  static String baseUrl = "";
  static String baseUrlMessenger = "";
  static const String appVersion = "1.6";

  static final String isIntroShowed = "isIntroShowed";
  static final String isJoinOrLoginShowed = "isJoinOrLoginShowed";


  static const String retry = "Retry";
  static const String connection_error = "Error while connecting to the server!";

  static const String MARK_ATTENDANCE_STR = "Mark Attendance";
  static const String MARK_ATTENDANCE_STR_ERROR = "Something went wrong.!";



  static final String authKey = "apiKey";
  static final String signup_authKey = "signup_authKey";
  static final String email = "email";
  static final String memberId = "memberId";
  static final String pleaseEnterValidEmail = "Please Enter Valid Email";
  static final String forgotpassword = "forgotpassword";
  static final String forgotpassword_email = "forgotpassword_email";
  static final String singup_personal_information = "singup_personal_information";
  static final String singup_professional_information = "singup_professional_information";
  static final String singup_contact_information = "singup_contact_information";

  static final Color TextFiledColor = Colors.grey[400];
  static final Color TextFiledFontColor = Colors.black54;
  static final Color TextFiledPrefixIconColor = Colors.black54;
  static final Color ButtonFontColor = Colors.grey[800];
  static final Color LabaleColor = Colors.black87;

  static final String cityName = "cityName";

  static String userAgreementUrl = "http://partner.api.elb."+baseEnvironment+".proxone.us-east-1.aws.rezgcorp.com/general/useragreement";
  static String privacyPolicyLink = "http://partner.api.elb."+baseEnvironment+".proxone.us-east-1.aws.rezgcorp.com/general/privacypolicy";

  static final String userAgreement = "This User Agreement ( “Agreement) constitutes a legally "
      "binding agreement between the User ( “You”) and Netxone (Private) Limited, a limited liability "
      "Company incorporated under the Laws of Sri Lanka bearing registration No. PV 00224751 and having its "
      "registered office at No.282/9A, Kotte Road, Pitakotte, Nugegoda (“Netxone”)."
      "Your use of the Application and the Service (defined hereinafter) and access to the website www.proxone.net, "
      "as well as all information, recommendations and/or services provided to You on or "
      "through the Application, the Service and the said Website, is subject exclusively "
      "to the Terms and Conditions of this Agreement.By clicking the “I ACCEPT” button or by "
      "downloading, installing, accessing or using  the Application and/or the "
      "Service or accessing the said Website, You agree to be bound by the "
      "Terms and Conditions of this Agreement and all laws and regulations applicable "
      "to the use of the Application, the Service and the website.";

  static final String privacyPolicy = "Netxone (Private) Limited ( “Us”, “We”, “Our”) will collect your information "
      "set out herein, when you access or use the Website, the Application and/or the Service."
      "Netxone will not use or share the said information with anyone, save and except in the "
      "manner and the in circumstances set out herein.This Privacy Policy forms part of the "
      "User Agreement which you enter into with Netxone and by entering into the said "
      "Agreement, you agree with Netxone’s collection, use and sharing of "
      "your information in accordance with this Privacy Policy."
      "The Definitions set out in the User Agreement will apply to this Privacy Policy.";

  static String introScreenOneTitle = "Welcome To";
  static String introScreenOne = "We are creating a whole new way of experiencing, achieving and maintaining a healthy lifestyle, by providing seamless access to a cross-section of Indoor and Outdoor activities globally.";
  static String introScreenTwoTitle = "Who We Are";
  static String introScreenTwo = "We are a Global online platform which brings about a total integration of services offered by the Fitness, Exercise, Sports, Recreation and Adventure industries.";
  static String introScreenThreeTitle = "What We Do";
  static String introScreenThree = "We make Indoor and Outdoor activities easily accessible, affordable, convenient to book and seamless to a cross-section of society on a “Pay-As-You-Go” basis.";



}

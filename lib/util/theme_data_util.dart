import 'package:flutter/material.dart';

class ThemeDataUtil {

  Color primaryColor = Colors.amber[600];
  Color accentColors = Colors.amber[300];

  ThemeData getTheme(primaryColor,accentColors) {

    return ThemeData(
      primaryColor: primaryColor,
      accentColor: accentColors,
    );

  }

  ThemeData getInitialTheme() {
    return ThemeData(
      brightness: Brightness.dark,
      primaryColor: primaryColor,
      accentColor: accentColors
    );
  }


}
